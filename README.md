# Recommendation service
One of microservices of Smart Fridge project for getting recipes based on the fridge content


## Setup:
Dependencies:
- docker


Steps:
```bash
#build image
$ docker-compose build
#start service
$ docker-compose up
```

Swagger:
- http://0.0.0.0:8008/docs#/
- http://0.0.0.0:8008/redoc
