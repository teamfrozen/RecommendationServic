
import pandas as pd
import sqlalchemy
from databases import Database
from sqlalchemy import create_engine, MetaData
from app.core.config import settings
from sqlalchemy.orm import sessionmaker, declarative_base

TESTING = settings.testing

if TESTING:
    DATABASE_URL = settings.database_url_testing
else:
    DATABASE_URL = settings.database_url
database = Database(DATABASE_URL)
metadata = MetaData()
engine = create_engine(
    DATABASE_URL,
)
metadata.create_all(engine)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)
Base = declarative_base()

try:
    csv_file_path = "app/static/csv_recipes/recipes.csv"
    with open(csv_file_path, 'r') as file:
        data_df = pd.read_csv(file)
    data_df.to_sql('recipes', con=engine, index=False, if_exists='append')
except sqlalchemy.exc.IntegrityError:
    pass