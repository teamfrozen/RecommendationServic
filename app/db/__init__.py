from .recipes import recipes
from .favoriteRecipes import favorite_recipes
from .base import metadata, engine

metadata.create_all(bind=engine)

