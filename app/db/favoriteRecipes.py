import sqlalchemy
from sqlalchemy.orm import relationship

from .base import metadata
import sqlalchemy_utils
from sqlalchemy.dialects.postgresql import UUID
from uuid import uuid4



favorite_recipes = sqlalchemy.Table("favorite_recipes",
                                    metadata,
                                    sqlalchemy.Column("favorite_user_recipe_id", UUID(), primary_key=True, unique=True),
                                    sqlalchemy.Column("user_id", UUID(), nullable=False),
                                    sqlalchemy.Column("recipe_id", UUID(),
                                                      nullable=False)
                                    )
