import sqlalchemy

from .base import metadata
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy_utils import URLType

recipes = sqlalchemy.Table("recipes",
                           metadata,
                           sqlalchemy.Column("recipe_id", UUID(), primary_key=True, unique=True),
                           sqlalchemy.Column("name", sqlalchemy.String, nullable=False),
                           sqlalchemy.Column("difficulty", sqlalchemy.String, nullable=False),
                           sqlalchemy.Column("hours", sqlalchemy.Integer, nullable=True),
                           sqlalchemy.Column("minutes", sqlalchemy.Integer, nullable=True),
                           sqlalchemy.Column("products", sqlalchemy.String, nullable=True),
                           sqlalchemy.Column("steps", sqlalchemy.String, nullable=False),
                           sqlalchemy.Column("imagePath", URLType, nullable=False))
