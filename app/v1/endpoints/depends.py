from app.core.repositories.recipes import RecipeRepository
from app.core.repositories.favoriteRecipes import FavoriteRecipeRepository
from app.core.repositories.recipesList import RecipesListRepository
from app.db.base import database


def get_recipes_repository() -> RecipeRepository:
    return RecipeRepository(database)


def get_favorite_recipes_repository() -> FavoriteRecipeRepository:
    return FavoriteRecipeRepository(database)


def get_recipes_list_repository() -> RecipesListRepository:
    return RecipesListRepository(database)
