from typing import List

from fastapi import APIRouter, Depends
from app.core.repositories.recipesList import RecipesListRepository
from app.core.models.recipe import RecipeIn
from uuid import UUID
from .depends import get_recipes_list_repository


router = APIRouter()

@router.get("/{user_id}", response_model=List[RecipeIn],
            summary="Get all user recipes by fridge content",
            description="Get all user recipes by fridge content. WARNING: does not work wo Smart fridge service.")
async def get_user_recipes(
        user_id: UUID,
        recipes: RecipesListRepository = Depends(get_recipes_list_repository)):
    return await recipes.get_proper_recipes(user_id)
