import shutil

from fastapi import APIRouter, Depends, HTTPException, status, UploadFile, File
from starlette.responses import FileResponse

from app.core.repositories.recipes import RecipeRepository
from app.core.models.recipe import Recipe, RecipeIn
from typing import List
from uuid import UUID, uuid4
from .depends import get_recipes_repository

router = APIRouter()


@router.get("/", response_model=List[RecipeIn],
            summary="Get all recipes",
            description="Get all recipes in database",
            response_model_exclude={"products", "steps"})
async def read_recipes(
        recipes: RecipeRepository = Depends(get_recipes_repository),
        limit: int = 100,
        skip: int = 0):
    return await recipes.get_all(limit, skip)


@router.get("/{recipe_id}",
            response_model=RecipeIn,
            summary="Get a recipe by id",
            description="Get a recipe by id")
async def get_recipe_by_id(
        recipe_id: UUID,
        recipes: RecipeRepository = Depends(get_recipes_repository)):
    recipe = await recipes.get_by_id(recipe_id=recipe_id)
    not_found_exception = HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Recipe not found")
    if recipe is None:
        raise not_found_exception
    return await recipes.get_by_id(recipe_id)


@router.post("/",
             response_model=RecipeIn,
             summary="Create a recipe",
             description="Create a recipe. If you want to add an image, it's necessary first upload an image")
async def create(recipe: Recipe, recipes: RecipeRepository = Depends(get_recipes_repository)):
    return await recipes.create(r=recipe)


@router.post("/uploadImage",
             response_model=UUID,
             summary="Upload an image for  recipe",
             description="Upload an image for  recipe")
async def upload_image(file: UploadFile = File(...)):
    image_uuid = str(uuid4())
    location = f"static/images/{image_uuid}.jpg"
    with open(location, "wb") as image:
        shutil.copyfileobj(file.file, image)
    return image_uuid


@router.get("/returnImage/{recipe_image_id}",
            summary="Return an image for  recipe",
            description="Return an image for  recipe")
async def return_image(recipe_image_id):
    return FileResponse(f"static/images/{recipe_image_id}.jpg")


@router.put("/{recipe_id}",
              response_model=RecipeIn,
              summary="Update the recipe",
              description="Update a recipe content")
async def update(recipe_id: UUID,
                 recipe: Recipe, recipes: RecipeRepository = Depends(get_recipes_repository)):
    found_recipe = await recipes.get_by_id(recipe_id=recipe_id)
    not_found_exception = HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Recipe not found")
    if found_recipe is None:
        raise not_found_exception
    return await recipes.update(recipe_id, r=recipe)


@router.delete("/{recipe_id}",
               summary="Delete the recipe",
               description="Delete the recipe")
async def delete(recipe_id: UUID,
                 recipes: RecipeRepository = Depends(get_recipes_repository)):
    found_recipe = await recipes.get_by_id(recipe_id=recipe_id)
    not_found_exception = HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Recipe not found")
    if found_recipe is None:
        raise not_found_exception
    result = await recipes.delete(recipe_id=recipe_id)
    return {"status": True}
