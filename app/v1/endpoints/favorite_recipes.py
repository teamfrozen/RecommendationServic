from fastapi import APIRouter, Depends, HTTPException, status
from app.core.repositories.favoriteRecipes import FavoriteRecipeRepository
from app.core.repositories.recipes import RecipeRepository
from app.core.models.favoriteRecipe import FavoriteRecipe, FavoriteRecipeIn
from typing import List
from uuid import UUID
from .depends import get_favorite_recipes_repository, get_recipes_repository

router = APIRouter()


@router.get("/{user_id}/favoriteRecipes",
            response_model=List[FavoriteRecipeIn],
            summary="Get all user favorite recipes",
            description="Get all user favorite recipes. "
                        "WARNING: does not work without Smart fridge service",
            response_model_exclude={"products", "steps"})
async def get_favorite_recipes(
        user_id: UUID,
        favorite_recipes: FavoriteRecipeRepository = Depends(get_favorite_recipes_repository)):
    not_found_exception = HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="User not found")
    response = favorite_recipes.check_user(user_id)
    try:
        if response["status"] == 404:
            raise not_found_exception
    except KeyError:
        pass
    return await favorite_recipes.get_all_by_user_id(user_id)


# @router.get("/{user_id}/getFavorite/{recipe_id}",
#             response_model=FavoriteRecipeIn,
#             summary="Get a favorite recipe by id",
#             description="Get a favorite recipe by id")
# async def get_favorite_recipe_by_id(
#         user_id: UUID,
#         recipe_id: UUID,
#         favorite_recipes: FavoriteRecipeRepository = Depends(get_favorite_recipes_repository)):
#     return await favorite_recipes.get_by_id(user_id=user_id, recipe_id=recipe_id)


@router.post("/{user_id}/makeFavorite/{recipe_id}",
             response_model=FavoriteRecipe,
             summary="Make the recipe favorite",
             description="Make the recipe favorite "
                         "WARNING: does not work without Smart fridge service"
             )
async def create(user_id: UUID, recipe_id: UUID,
                 favorite_recipes: FavoriteRecipeRepository = Depends(get_favorite_recipes_repository),
                 recipes: RecipeRepository = Depends(get_recipes_repository)):
    not_found_exception = HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="User not found")
    response = favorite_recipes.check_user(user_id)
    try:
        if response["status"] == 404:
            raise not_found_exception
    except KeyError:
        pass
    recipe = await recipes.get_by_id(recipe_id=recipe_id)
    not_found_exception = HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Recipe not found")
    if recipe is None:
        raise not_found_exception
    return await favorite_recipes.create(user_id=user_id, recipe_id=recipe_id)


@router.delete("/{user_id}/deleteFromFavorite/{recipe_id]",
               summary="Delete the recipe from favorites",
               description="Delete the recipe from favorites"
                           "WARNING: does not work without Smart fridge service")
async def delete(user_id: UUID,
                 recipe_id: UUID,
                 favorite_recipes: FavoriteRecipeRepository = Depends(get_favorite_recipes_repository)):
    favorite_recipe = await favorite_recipes.get_by_id(user_id=user_id, recipe_id=recipe_id)
    not_found_exception = HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Favorite recipe not found")
    if favorite_recipe is None:
        raise not_found_exception
    result = await favorite_recipes.delete(user_id=user_id, recipe_id=recipe_id)
    return {"status": True}
