import json
from uuid import UUID

from pydantic import BaseModel, conint
from typing import List, Optional, Union
from pathlib import Path
from enum import Enum


class DifficultyEnum(str, Enum):
    """ Difficult choices"""
    high = "high",
    medium = "medium"
    low = "low",

class Recipe(BaseModel):
    name: str
    difficulty: DifficultyEnum = DifficultyEnum.low
    hours: conint(ge=0, lt=24)
    minutes: conint(ge=0, lt=60)
    products: str
    steps: str
    imagePath: str = "recipes/returnImage/default"


class RecipeIn(Recipe):
    recipe_id: Union[UUID, int, str]
