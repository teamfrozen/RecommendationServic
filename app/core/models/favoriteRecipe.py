from uuid import UUID

from pydantic import BaseModel
from typing import Union
from app.core.models.recipe import RecipeIn


class FavoriteRecipe(BaseModel):
    favorite_user_recipe_id: Union[UUID, int, str]
    user_id: Union[UUID, int, str]
    recipe_id: Union[UUID, int, str]


class FavoriteRecipeIn(RecipeIn):
    favorite: bool = True

    class Config:
        orm_mode = True

