from uuid import UUID, uuid4

from app.db.recipes import recipes
from app.core.models.recipe import Recipe, RecipeIn
from .base import BaseRepository
from typing import List


class RecipeRepository(BaseRepository):
    """
    Describes recipe's behaviour
    """

    async def get_all(self, limit: int = 100, skip: int = 0) -> List[RecipeIn]:
        query = recipes.select().limit(limit).offset(skip)
        return await self.database.fetch_all(query=query)

    async def get_by_id(self, recipe_id: UUID) -> RecipeIn:
        query = recipes.select().where(recipes.c.recipe_id == str(recipe_id))
        recipe = await self.database.fetch_one(query)
        if recipe is None:
            return None
        return recipe

    async def create(self, r: Recipe) -> RecipeIn:
        recipeIn = RecipeIn(
            recipe_id=uuid4(),
            name=r.name,
            difficulty=r.difficulty,
            hours=r.hours,
            minutes=r.minutes,
            products=r.products,
            steps=r.steps,
            imagePath=r.imagePath
        )
        values = {**recipeIn.dict()}
        query = recipes.insert().values(**values)
        await self.database.execute(query)
        return recipeIn

    async def updateImage(self, url):
        pass

    async def update(self, recipe_id: UUID, r: Recipe) -> RecipeIn:
        recipeIn = RecipeIn(
            recipe_id=recipe_id,
            name=r.name,
            difficulty=r.difficulty,
            hours=r.hours,
            minutes=r.minutes,
            products=r.products,
            steps=r.steps,
            imagePath=r.imagePath,
        )
        values = {**recipeIn.dict()}
        query = recipes.update().where(recipes.c.recipe_id == recipe_id).values(**values)
        await self.database.execute(query)
        return recipeIn

    async def delete(self, recipe_id: UUID):
        query = recipes.delete().where(recipes.c.recipe_id == str(recipe_id))
        return await self.database.execute(query=query)
