from json import JSONDecodeError

from .base import BaseRepository
import requests
from app.core.config import settings
from app.db.recipes import recipes


class RecipesListRepository(BaseRepository):

    def get_all_products(self, user_id) -> list:
        response = requests.get(f"{settings.fridge_service_address}/Fridges/OwnerContents/{user_id}")
        productTypesInFridge = []
        try:
            productTypesInFridge = [type["productTypeName"].lower() for type in response.json()]
        except (JSONDecodeError, TypeError):
            pass
        return productTypesInFridge

    async def get_proper_recipes(self, user_id):
        productTypesInFridge = self.get_all_products(user_id)
        all_recipes = await self.database.fetch_all(recipes.select())
        proper_recipes = []
        for recipe in all_recipes:
            if set(recipe["products"].split(",")).intersection(productTypesInFridge):
                proper_recipes.append(recipe)
        return proper_recipes