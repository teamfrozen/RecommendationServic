from json import JSONDecodeError

from databases import Database
import requests

from app.core.config import settings


class BaseRepository:
    def __init__(self, database: Database):
        self.database = database

    def check_user(self, user_id):
        response = requests.get(f"{settings.fridge_service_address}/Users/{user_id}")
        return response.json()
