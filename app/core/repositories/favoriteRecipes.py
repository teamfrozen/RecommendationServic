from uuid import UUID, uuid4

from app.db.favoriteRecipes import favorite_recipes
from app.db.recipes import recipes
from app.core.models.favoriteRecipe import FavoriteRecipe,FavoriteRecipeIn
from .base import BaseRepository
from typing import List


class FavoriteRecipeRepository(BaseRepository):

    async def get_all_by_user_id(self, user_id: UUID) -> List[FavoriteRecipeIn]:
        query = favorite_recipes.join(recipes).select().where(favorite_recipes.c.user_id == user_id)
        favorite_user_recipes = await self.database.fetch_all(query)
        parsed_recipes = [FavoriteRecipeIn.parse_obj(r) for r in favorite_user_recipes]
        return parsed_recipes

    async def get_by_id(self, user_id: UUID, recipe_id: UUID) -> FavoriteRecipeIn:
        query = favorite_recipes.join(recipes).select().where(
            favorite_recipes.c.user_id == user_id, favorite_recipes.c.recipe_id == recipe_id)
        recipe = await self.database.fetch_one(query)
        if recipe is None:
            return None
        return FavoriteRecipeIn.parse_obj(recipe)

    async def create(self, user_id: UUID, recipe_id: UUID) -> FavoriteRecipe:
        favorite_recipe = FavoriteRecipe(
            favorite_user_recipe_id=uuid4(),
            user_id=user_id,
            recipe_id=recipe_id,
        )
        values = {**favorite_recipe.dict()}
        query = favorite_recipes.insert().values(**values)
        await self.database.execute(query)
        return favorite_recipe

    async def delete(self, user_id: UUID, recipe_id: UUID):
        query = favorite_recipes.delete().where(
            favorite_recipes.c.user_id == user_id, favorite_recipes.c.recipe_id == recipe_id)
        return await self.database.execute(query=query)
