import os
from dotenv import load_dotenv
from pydantic import BaseSettings

load_dotenv('.env')


class Settings(BaseSettings):
    app_name: str = "Recommendation service"
    admin_email: str = "sabinabeliaeva@gmail.com"
    server_address:str = os.environ.get("RECOMMEND_SERV_ADD")
    port:  str = os.environ.get("RECOMMEND_SERV_PORT")
    fridge_service_address: str = os.environ.get('APP_FRIDGE_SERVICE_URL')
    testing: bool = bool(os.environ.get("TESTING"))
    database_url: str = os.environ.get("DATABASE_URL")
    database_url_testing: str = os.environ.get("DATABASE_URL_TESTING")

settings = Settings()

