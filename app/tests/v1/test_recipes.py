import os
from uuid import uuid4

import pytest
from app.main import app
from starlette.testclient import TestClient


"""Перед запуском помеять переменную среды TESTING = True"""

@pytest.fixture(scope="module")
def pytest_configure():
    pytest.recipe_id = None


@pytest.fixture(scope="module")
def client():
    with TestClient(app) as c:
        yield c


def test_create_recipe(client, pytest_configure):
    response = client.post(
        "/recipes/",
        json={
            "name": "soup",
            "difficulty": "medium",
            "hours": 2,
            "minutes": 0,
            "products": [
                "chicken",
                "onions"
            ],
            "steps": [
                "string"
            ],
            "imagePath": "recipes/returnImage/default"
        }
    )
    assert response.status_code == 200
    assert response.json()['name'] == "soup"
    assert response.json()['difficulty'] == "medium"
    assert response.json()['hours'] == 2
    assert response.json()['minutes'] == 0
    assert response.json()['products'] == ["chicken", "onions"]
    assert response.json()['steps'] == ["string"]
    assert response.json()['imagePath'] == "recipes/returnImage/default"
    pytest.recipe_id = response.json()["recipe_id"]


def test_wrong_difficulty_create_test(client, pytest_configure):
    response = client.post(
        "/recipes/",
        json={
            "name": "test",
            "difficulty": "mediums",
            "hours": 7,
            "minutes": 0,
            "products": [
                "chicken",
                "onions"
            ],
            "steps": [
                "string"
            ],
            "imagePath": "recipes/returnImage/default"
        }
    )
    assert response.status_code == 422

def test_negative_hours_create_test(client, pytest_configure):
    response = client.post(
        "/recipes/",
        json={
            "name": "test",
            "difficulty": "mediums",
            "hours": -8,
            "minutes": 0,
            "products": [
                "chicken",
                "onions"
            ],
            "steps": [
                "string"
            ],
            "imagePath": "recipes/returnImage/default"
        }
    )
    assert response.status_code == 422

def test_get_recipe(client, pytest_configure):
    response = client.get(
        f"/recipes/{pytest.recipe_id}",
    )
    assert response.status_code == 200
    assert response.json()['name'] == "soup"
    assert response.json()['difficulty'] == "medium"
    assert response.json()['hours'] == 2
    assert response.json()['minutes'] == 0
    assert response.json()['products'] == ["chicken", "onions"]
    assert response.json()['steps'] == ["string"]
    assert response.json()['imagePath'] == "recipes/returnImage/default"


def test_update_recipe(client, pytest_configure):
    response = client.put(
        f"/recipes/{pytest.recipe_id}",
        json={
            "name": "soup",
            "difficulty": "medium",
            "hours": 2,
            "minutes": 30,
            "products": [
                "chicken",
                "onions"
            ],
            "steps": [
                "cook the soup"
            ],
            "imagePath": "recipes/returnImage/default"
        }
    )
    assert response.status_code == 200
    assert response.json()['name'] == "soup"
    assert response.json()['difficulty'] == "medium"
    assert response.json()['hours'] == 2
    assert response.json()['minutes'] == 30
    assert response.json()['products'] == ["chicken", "onions"]
    assert response.json()['steps'] == ["cook the soup"]
    assert response.json()['imagePath'] == "recipes/returnImage/default"

def test_wrong_difficulty_update_test(client, pytest_configure):
    response = client.put(
        f"/recipes/{pytest.recipe_id}",
        json={
            "name": "test",
            "difficulty": "mediums",
            "hours": 7,
            "minutes": 0,
            "products": [
                "chicken",
                "onions"
            ],
            "steps": [
                "string"
            ],
            "imagePath": "recipes/returnImage/default"
        }
    )
    assert response.status_code == 422

def test_negative_hours_update_test(client, pytest_configure):
    response = client.put(
        f"/recipes/{pytest.recipe_id}",
        json={
            "name": "test",
            "difficulty": "medium",
            "hours": -7,
            "minutes": 0,
            "products": [
                "chicken",
                "onions"
            ],
            "steps": [
                "string"
            ],
            "imagePath": "recipes/returnImage/default"
        }
    )
    assert response.status_code == 422

def test_delete_recipe(client, pytest_configure):
    response = client.delete(
        f"/recipes/{pytest.recipe_id}")
    assert response.status_code == 200

def test_delete_recipe_not_found(client, pytest_configure):
    random_uuid = uuid4()
    response = client.delete(
        f"/recipes/{random_uuid}")
    assert response.status_code == 404


def test_get_recipe_not_found(client):
    random_uuid = uuid4()
    response = client.get(
        f"/recipes/{random_uuid}",
    )
    assert response.status_code == 404
