import logging
from fastapi import FastAPI
import uvicorn
from app.db.base import database, SessionLocal
from app.core.config import settings
from app.v1.endpoints import favorite_recipes, recipes_list, recipes

logger = logging.getLogger("main")
logger.setLevel(logging.DEBUG)

app = FastAPI(title="Recommendation service")
app.include_router(recipes.router, prefix="/recipes", tags=["recipes"])
app.include_router(favorite_recipes.router, prefix="/recipes/favorites", tags=["favorites"])
app.include_router(recipes_list.router, prefix="/recipes/userListRecipes", tags=["recipesList"])


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


@app.get("/info")
async def info():
    return {
        "app_name": settings.app_name,
        "admin_email": settings.admin_email,
    }


@app.on_event("startup")
async def startup():
    await database.connect()


@app.on_event("shutdown")
async def shutdown():
    await database.connect()


if __name__ == "__main__":
    uvicorn.run("main:app", port=settings.port, host=settings.server_address, reload=True)
