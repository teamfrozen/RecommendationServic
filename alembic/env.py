from logging.config import fileConfig
from os import environ
from alembic import context
from app.db import favorite_recipes, recipes

# Alembic Config объект предоставляет доступ
# к переменным из файла alembic.ini
config = context.config

section = config.config_ini_section
config.set_section_option(section, "DB_USER", environ.get("DB_USER"))
config.set_section_option(section, "DB_PASS", environ.get("DB_PASS"))
config.set_section_option(section, "DB_NAME", environ.get("DB_NAME"))
config.set_section_option(section, "DB_HOST", environ.get("RECOMMEND_SERV_ADD"))

fileConfig(config.config_file_name)

target_metadata = [favorite_recipes.metadata, recipes.metadata]